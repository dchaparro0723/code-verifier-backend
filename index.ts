import express, { Express, Request, Response } from "express"; 
import dotenv from "dotenv";
import { endianness } from "os";
import { env } from "process";

// configuration the .env file
dotenv.config();

// Create Express APP
const app: Express =  express();
const port: string | number = process.env.PORT || 8000;

// Define the first route
app.get('/', (req: Request, res: Response) => {
    // Send "hello world!"
    res.send('Welcome to API restful Express Mongoose Node and Swagger');
})

// Define the first route
app.get('/hello', (req: Request, res: Response) => {
    // Send "hello world!"
    res.send('Welcome Hello World second route');
})

// Define de second route
app.get('/godbye', (req: Request, res: Response) => {
    // Send first JSON
    console.log("Mostrar msj");
    res.send(env.godbye)
})

// Execute the APP Listen Request to Port
app.listen(port, ()=> console.log(`Express Server: Running at HTTP://localhost:${port}`))





