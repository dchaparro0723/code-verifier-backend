"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
const process_1 = require("process");
// configuration the .env file
dotenv_1.default.config();
// Create Express APP
const app = (0, express_1.default)();
const port = process.env.PORT || 8000;
// Define the first route
app.get('/', (req, res) => {
    // Send "hello world!"
    res.send('Welcome to API restful Express Mongoose Node and Swagger');
});
// Define the first route
app.get('/hello', (req, res) => {
    // Send "hello world!"
    res.send('Welcome Hello World second route');
});
// Define de second route
app.get('/godbye', (req, res) => {
    // Send first JSON
    console.log("Mostrar msj");
    res.send(process_1.env.godbye);
});
// Execute the APP Listen Request to Port
app.listen(port, () => console.log(`Express Server: Running at HTTP://localhost:${port}`));
//# sourceMappingURL=index.js.map